============
Get iDiffIR
============

Required packages
=================
The following packages are required for iDiffIR core functions.
Version numbers correspond to those tested during development.

- SpliceGrapher_ (v0.2.4 )
- SciPy_ (v0.14.0)
- Pysam_ (v0.8.1)
- matplotlib_ (v1.3.1)
- NumPy_ (v1.9.0)




Download
========
current release: v0.3.1_

iDiffIR is hosted on bitbucket https://bitbucket.org/comp_bio/idiffir

Install 
=======
.. code-block:: none

		$ tar zxvf iDiffIR_<version>.tgz
		$ cd iDiffIR_<version>.tgz
		$ python setup.py install

.. note::

   To install in a user directory, use the option:

   .. code-block:: none

		   --home=/Path/To/Local/Library

      

.. _SpliceGrapher: http://splicegrapher.sourceforge.net/
.. _SciPy: http://www.scipy.org/scipylib/index.html
.. _Pysam: https://code.google.com/p/pysam/
.. _matplotlib: http://matplotlib.org/
.. _NumPy: http://www.numpy.org/
.. _v0.3.1: _static/iDiffIR_v0.3.1.tgz
